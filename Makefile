SHELL = /bin/bash
.SUFFIXES :
.SUFFIXES : .f .o
src = ./src/
build = ./obj/
bin = ./bin/

######################################################################
version=localdw-1.0
######################################################################

#IFORT VERSION (DEFAULT)
FC = ifort
FFLAGS = -O2 -openmp -mkl -heap-arrays -module $(build) -cpp
#-openmp -complex-limited-range -xW -i-static  -ip -ftz -no-prec-div  -opt-prefetch -heap-arrays -align dcommons -mkl -mcmodel=medium
DBGFLAGS = -debug -check -check bounds #-warn uncalled -warn nousage -warn nounused -openmp -warn -warn notruncated_source
DBGFLAGS+= -pg

#MODERN IFORT VERSION (for compiling on laptops)
NEWFFLAGS =-O2 -qopenmp -qmkl -heap-arrays -module $(build) -cpp -g

#GFORTRAN (INVOKED VIA MAKE GFORTRAN)
GNUFC = gfortran #You can get newer versions of gfortran, if you perform "scl enable devtoolset-10 bash" in your shell first
GNUQFC = /opt/rh/devtoolset-10/root/bin/gfortran
GNUFFLAGS =  -O3 -ffast-math -march=native -p -opt-prefetch -fopenmp -std=legacy -llapack -cpp -J$(build) #Note that for new version of gfortran you might have to add -std=legacy or -fallow-argument-mismatch to compile random.f without errors!
#-fallow-argument-mismatch
GNUDBGFLAGS =   -fcheck=bounds -fcheck=do -fcheck=mem -fcheck=pointer  -p -Og #-gdwarf-5 -O0 -Wall

#MPI VERSION (INVOKED VIA MAKE MPI)
MPIFC=mpif90
MPIFFLAGS = -fcx-limited-range -O3 -ffast-math -march=native -p -opt-prefetch -falign-commons -mcmodel=large -fopenmp  -J$(build) -llapack -cpp -Dmpi_version #TODO: Check if all these flags are necessary!
#Syntax for running mpi calculations:
# - 1 machine with 12 cores: mpirun -np 12 genetic test.genetic
# - 4 machine with 12 cores: mpirun -np 48 --hostfile nodes.txt genetic test.genetic
# - nodes.txt specifies the nodes on which the program will run, the first mentioned note will perform the master thread
# - you have to start the calculation from the node with the master thread and have running sleep jobs for the other notes
# - TODO: Write a job file / submission script that automatizes this procedure

#mpirun -np 48 --hostfile nodes.txt genetic s_test-dist9-freeze.genetic > s_test-dist9-freeze.out &

######################################################################

#Extend search path for files (both .f and .incl files)
VPATH += $(src)
VPATH += $(src)parser
VPATH += $(src)parser/lib
VPATH += $(src)model

######################################################################

#Define objects for different Program parts (sorted in order of compilation)
parserlib_obj = strings.o long_keyread.o fileread.o keyread.o long_write.o
parser_obj = io_parameters.o keys.o dim_parameter.o parameterkeys.o parse_errors.o parser.o

datamodule_obj = data_module.o #Compile this module before your model files and the genetic files

model_obj = ptr_structure.o ctrans.o model.o weight.o adia.o

mod_incl = mod_const.incl so_param.incl

random_obj = $(addprefix $(build), random.o)

genetic_obj = data_transform.o init.o write.o funcs.o marq.o lbfgsb.o idxsrt_mod.o fit_MeX.o mpi_fit_MeX.o genetic.o #content of data_transform and write is user specific, interfaces are fixed

objects = $(addprefix $(build), $(parserlib_obj) $(parser_obj) $(datamodule_obj) $(model_obj) $(genetic_obj) )

#Note: Since we are using modules, you have carefully choose the order of compilation and take dependencies between modules and subroutines into account!

######################################################################

# define main goal
main = genetic


# define main compilation
gfortran: override FC = $(GNUFC)
gfortran: override FFLAGS = $(GNUFFLAGS)
gfortran: $(main)

$(main) : dirs $(random_obj) $(objects)
	$(FC) $(FFLAGS) $(random_obj) $(objects) -o $(bin)$(main)
$(build)%.o : %.f
	$(FC) -c  $(FFLAGS) $^ -o $@


$(model_obj) : $(mod_incl)
######################################################################

# define name of additional recipes
.PHONY: clean neat remake debug test mpi gfortran gqfortran profile tar dirs

# define additionational recipes
trash= *__genmod* $(addprefix $(build),*__genmod* *.mod)
clean:
	$(RM) $(objects) $(trash)

neat: clean
	$(RM) $(random_obj)

remake: clean $(main)

dirs:
	@mkdir -p $(build) $(bin)

debug: override FFLAGS += $(DBGFLAGS)
debug: clean $(main)
	cp $(infile) $(bin)
	$(bin)$(main) $(bin)$(infile) | tee debug.out

modern: override FFLAGS = $(NEWFFLAGS)
modern: $(main)

gqfortran: override FC = $(GNUQFC)
gqfortran: override FFLAGS = $(GNUFFLAGS)
gqfortran: $(main)

gdebug: override FC = $(GNUFC)
gdebug: override FFLAGS = $(GNUFFLAGS) $(GNUDBGFLAGS)
gdebug: clean $(main)

mpi: override FC = $(MPIFC)
mpi: override FFLAGS = $(MPIFFLAGS)
mpi: $(main)

infile=hi-sing1-sig.genetic

gtest: override FC = $(GNUFC)
gtest: override FFLAGS = $(GNUFFLAGS)
gtest: clean $(main)
	cp $(infile) $(bin)
	$(bin)$(main) $(bin)$(infile) | tee test.out

gprofile: override FC = $(GNUFC)
gprofile: override FFLAGS = $(GNUFFLAGS) -pg
gprofile: clean $(main)
	cp $(infile) $(bin)



test: clean $(main)
	cp $(infile) $(bin)
	$(bin)$(main) $(bin)$(infile) | tee test.out

profile: override FFLAGS += -pg
profile: clean test
	date > profile
	gprof $(bin)$(main) gmon.out >> profile

timestamp=$(shell date +"%FT%H-%M-%S")
tar:
	date > INFO
	tar --exclude-backups --exclude-vcs -czf tardir/geneticsrc_$(timestamp).tar src/ obj/ bin/ Makefile INFO
