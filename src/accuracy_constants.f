      module accuracy_constants
      use iso_fortran_env
      implicit none
!      integer, parameter :: racc = real32  !real*4
      integer, parameter :: racc = real64  !real*8
!      integer, parameter :: racc = real128 !real*16

!      integer, parameter :: iacc = int16 !int*2
      integer, parameter :: iacc = int32 !int*4
!      integer, parameter :: iacc = int64 !int*8
      end module
