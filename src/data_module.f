      module data_module

      implicit none

      double precision,protected, dimension(:,:), allocatable :: q_m
      double precision,protected, dimension(:,:), allocatable :: x1_m
      double precision,protected, dimension(:,:), allocatable :: x2_m
      double precision,protected, dimension(:,:), allocatable ::  y_m
      double precision,protected, dimension(:,:), allocatable :: wt_m
      double precision,protected, dimension(:,:), allocatable :: ny_m

      contains

      !------------------------------

      subroutine init_data(numdatpt,q,x1,x2,y,wt,ny)

      use dim_parameter, only: qn, ntot

      implicit none

      integer i,numdatpt
      double precision q(qn,*)
      double precision x1(qn,*)
      double precision x2(qn,*)
      double precision y(ntot,*)
      double precision wt(ntot,*)
      double precision ny(ntot,*)

      allocate(q_m(qn,numdatpt))
      allocate(x1_m(qn,numdatpt))
      allocate(x2_m(qn,numdatpt))
      allocate(y_m(ntot,numdatpt))
      allocate(wt_m(ntot,numdatpt))
      allocate(ny_m(ntot,numdatpt))

      do i=1,numdatpt
         q_m(1:qn,i)=q(1:qn,i)
         x1_m(1:qn,i)=x1(1:qn,i)
         x2_m(1:qn,i)=x2(1:qn,i)
         y_m(1:ntot,i)=y(1:ntot,i)
         wt_m(1:ntot,i)=wt(1:ntot,i)
         ny_m(1:ntot,i)=ny(1:ntot,i)
      enddo

      end subroutine

      !------------------------------

      subroutine dealloc_data()
      deallocate(q_m,x1_m,x2_m,y_m,wt_m,ny_m)
      end subroutine

      end module data_module
