      module dim_parameter
      use io_parameters,only: maxpar_keys
      implicit none
      integer,parameter :: max_ntot = 200 ,max_par = 600
      !Standard
      integer :: qn,qn_read,ntot,numdatpt
      integer :: nstat,ndiab,nci
      !Fabian
!      integer :: numdatpt
!      integer,parameter :: qn=9,ntot=162,nstat=8,ndiab=22,nci=7

      integer :: sets
      integer, allocatable :: ndata(:)
      logical :: hybrid, anagrad,lbfgs
      integer :: lbfgs_corr
      double precision :: facspread
      logical :: log_convergence
!     Weight Parameter
      double precision :: wt_en2ci
      double precision, allocatable ::  wt_en(:),wt_ci(:) !< parameters for weightingroutine, nstat or ndiab long
!     which coord to use for plotting
      integer, allocatable ::  plot_coord(:)

!     pst vector
      integer pst(2,maxpar_keys)

!     thresholds for error calculation
      double precision, allocatable :: rms_thr(:)

      contains

      subroutine dealloc_dim()
      deallocate(ndata,wt_ci,wt_en,rms_thr,plot_coord)
      end subroutine

      end module
