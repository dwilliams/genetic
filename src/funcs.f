      module funcs_mod
      implicit none
      logical,parameter:: dbg =.false.
      double precision, parameter:: thr_grad_diff = 1.d-3
      contains

      subroutine funcs(n,p,ymod,dymod,npar,p_act,skip)
!     use dim_parameter,only:ntot,ndiab,anagrad
      use dim_parameter,only:ntot,ndiab,anagrad,nstat,nci !Fabian
      use data_module,only: x1_m
      use adia_mod,only: adia
!     In variables
      integer n, npar, p_act(npar)
      double precision ymod(ntot)
      double precision p(npar)
      logical skip
!     out variables
      double precision dymod(ntot,npar)
      double precision dum_dymod(ntot,npar)
      logical diff(ntot,npar)
!     internal varibales
      double precision ew(ndiab),ev(ndiab,ndiab) ! eigenvalues(ew) and eigenvectors(ev)
      integer i,j
      logical,parameter:: dbg =.false.

      skip=.false.
      diff=.false.
!     get adiabatic energies:
      call adia(n,p,npar,ymod,ew,ev,skip)
      if(skip) return

      if(eigchk(ew,nci)) then   !Fabian: since pseudo-inverse is only calculated for first nci eigenvalues and their ci-vectors, if changed the check to nci
         dymod = 0.d0
         if(dbg) write(6,*)'funcs skipping point,n: ',n
         return
      endif
!     compute gradient with respect to parameter vector:
      if(anagrad) then
         write(6,*) 'ERROR: NOT SUPPORTED.'
         stop
      else
!        compute gradients numerically
         call num_grad(dymod,n,p,npar,p_act,skip)
      endif
      end subroutine

!----------------------------------------------------------------------
!     compute gradient of adiabatic energies nummerically with respect to parameters:
      subroutine num_grad(dymod,n,p,npar,p_act,skip)
      use dim_parameter,only: ntot,ndiab
      use adia_mod,only: adia
      integer n, i, j, npar
      integer p_act(npar)
      double precision ymod(ntot), dymod(ntot,npar), p(npar)
      double precision dp(npar)
      logical skip
      double precision ew(ndiab),ev(ndiab,ndiab)
!     determine finite differences for each parameter:
      call pdiff(p,dp,npar)

!     generate numerical gradients for all parameters individually
      do i=1,npar

         do j=1,ntot
            dymod(j,i)=0.d0
         enddo

!        calculate gradient for active parameter, for inactive parameter gradient is always zero
!     Nicole: added flexible value of p_act
         if (p_act(i).ge.1) then

!           change parameter in forward direction
            p(i)=p(i)+dp(i)
            call adia(n,p,npar,ymod,ew,ev,skip)
            if (skip) then
               p(i)=p(i)-dp(i)
               return
            endif
            do j=1,ntot
               dymod(j,i)=ymod(j)
            enddo

!           change parameter in backward direction
            p(i)=p(i)-2.d0*dp(i)
            call adia(n,p,npar,ymod,ew,ev,skip)
            if (skip) then
               p(i)=p(i)+2.d0*dp(i)
               return
            endif
            do j=1,ntot
               dymod(j,i)=(dymod(j,i)-ymod(j))/(2.d0*dp(i)) !Form symmetric difference quotient
            enddo

!           restore original parameter
            p(i)=p(i)+dp(i)
         endif
      enddo
      end subroutine num_grad
!----------------------------------------------------------------------
!     determine appropriate finite differences for each parameter:
      subroutine pdiff(p,dp,npar)
      integer i, npar
      double precision p(npar), dp(npar)
!      double precision, parameter :: d = 1.d-4
      double precision, parameter :: d = 1.d-6 !Standard
!     double precision, parameter :: d = 1.d-8
      double precision, parameter :: thr = 1.d-12
      do i=1,npar
         dp(i)=abs(p(i)*d)
         if (dp(i).lt.thr) dp(i)=thr
      enddo
      end subroutine pdiff

!--------------------------------------------------------------------------------------
!..   check vector of eigenvalues for (near) degeneragies
      logical function eigchk(v,n)
!..   on input:
      integer n
      double precision v(n)
!..   local variables:
      double precision thr
      parameter (thr=1.d-8)     !threshold for degeneracy
      integer j
      eigchk=.false.
      do j=1,n-1
         if (abs((v(j+1)-v(j))).lt.thr) then
            eigchk=.true.
            return
         endif
      enddo
      end function eigchk
      end module funcs_mod
