      module idxsrt_mod
      implicit none
      contains
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!     % SUBROUTINE IDXSRT(...)
!     %
!     % indices are sorted by ascending values of x, that means if you go
!     % throug x(idx(1..n)) from one to n, you will get an list of growing
!     % values
!     %
!     % variables:
!     % idx: indeces which are going to be sorted(int[n])
!     % n:   number of indices (int)
!     % x:   array of values (real[n]))
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      subroutine idxsrt(x,idx,n)
      integer i, j, k, n, idx(n)
      double precision x(n), dum
      do i=1,n
         idx(i)=i
      enddo
      do i=1,n
         do j=i+1,n
            if (x(j).lt.x(i)) then
               dum=x(i)
               x(i)=x(j)
               x(j)=dum
               k=idx(i)
               idx(i)=idx(j)
               idx(j)=k
            endif
         enddo
      enddo
      end subroutine idxsrt
      end module idxsrt_mod
