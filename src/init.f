      module init_mod
      implicit none
      contains

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!% SUBROUTINE RINIT
!%
!% Subroutine to define the allowed range for each parameter:
!% for the moment this is a distribution around zero with a given width
!% for each parameter
!%
!% Input variables:
!% par:    Parameter vectot (double[])
!% spread: Spread of each parameter (double[])
!% ma:     Active cards for every parameter (int[])
!% npar:   Number of Parameters
!%
!% Output variables
!% prange: Spread interval vector (double[])
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      subroutine rinit(par,prange,p_spread,p_act,npar)
      implicit none
      integer i,npar,p_act(npar)
      double precision par(npar), prange(2,npar), p_spread(npar),de,dum
      !minimum absolute spread
      double precision minspread
      parameter(minspread=1.d-4)

      do i=1,npar
         if (abs(p_act(i)).eq.0) p_spread(i)=0.d0
         dum=par(i)
         if (abs(dum).lt.1.d-6) dum=minspread
         de=abs(dum*p_spread(i)/2.d0)
         prange(1,i)=par(i)-de
         prange(2,i)=par(i)+de
      enddo

      end subroutine

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!% SUBROUTINE PINIT(...)
!%
!% subroutine to initialize the nset parameter sets with random
!% numbers in the range defined by prange
!%
!% Input Variables:
!% par:    parameter vector (double[])
!% prange: Spread interval vector (double[])
!% npar:   number of parameters (int)
!% nset:   number of sets (int)
!% seed:   seed for random.f (int)
!% gtype:  selects random number generator (int)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      subroutine pinit(par,prange,npar,nset,seed,gtype)
      implicit none

      integer i, j, npar, nset, seed, gtype,cont
      double precision par(npar,nset), prange(2,npar), rn, dum

!..   initialize new random number stream:
      cont=1
      dum=rn(seed,gtype,cont)

!..   create all the parameter sets by random numbers
      !continue with the initialized random number stream
      cont=0
      do i=2,nset
         do j=1,npar
            par(j,i)=prange(1,j)+rn(seed,gtype,cont) *
     $        (prange(2,j)-prange(1,j))
         enddo
      enddo
      end subroutine



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!% SUBROUTINE ACTINIT(...)
!%
!% subroutine to select the active parameters and assign their indices
!%
!% Input Variables:
!% p_act: vector of active cards
!% npar:  total number of parameters
!%
!% Output Variables:
!% iact: list of active parameters
!% mfit: number of active parameters
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      subroutine actinit(p_act,iact,mfit,npar)
      implicit none

      integer i, npar, p_act(npar), iact(npar), mfit

      mfit=0
      iact=0
      do i=1,npar
!     Nicole: added flexible value of p_act
         if (p_act(i).ge.1) then
           mfit=mfit+1
           iact(mfit)=i
        endif
      enddo

      end subroutine

      end module init_mod
