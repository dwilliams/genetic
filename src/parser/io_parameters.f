      module io_parameters
      implicit none
!     ******************************************************************************
!     ****  I/O-Parameters
!     ***
!     *** dnlen:     maximum char length of data file path
!     *** maxlines:  maximum input file length
!     *** llen:      character maximum per line
!     *** maxdat:    maximum number of input data values of one kind
!     ***            (e.g. integer values) excluding DATA: block
!     *** clen:      max. character length of string data
!     *** klen:      maximum length of key or typestring
!     *** maxkeys:   max. number of keys
!     *** maxerrors: max. number of pre-defined error messages.

      integer, parameter  :: dnlen = 8192
      integer, parameter  :: maxlines = 3000000,llen = 750
      integer, parameter  :: klen=20,maxkeys=200
      integer, parameter  :: maxdat=2000,clen=1024
      integer, parameter  :: maxerrors=100
!     Declarations for general Keylist and error massages
      integer             :: keynum !< keynum number of general  keys
      integer             :: datpos(3,maxdat) !< datpos Pointer to type, data adress and length for each general key
      character(len=klen) :: keylist(2,maxkeys) !< list of general program keys for programm control and parameter initialisation defined in keylist.incl
      character(len=64)   :: errcat(maxerrors) !< list of generic error Messages defined in errcat.incl

!     parameter key declaration
      integer, parameter  :: maxpar_keys=400    !<maximum number of parameter keys
      character(len=klen) :: key(4,maxpar_keys) !<list of parameter keys (1-4: number,value,active?,spread)
      integer             :: parkeynum          !< actual number of parameterkeys specified
      integer             :: parkeylen          !< lenght of longest parameterkey string

!**********************************************************                                                                                                                                                                            
!**** Error Codes                                                                                                                                                                                                                      
!*** Codes should be powers of 2.  Binary representation of return value                                                                                                                                                               
!*** should correspond to all exceptions invoked.  ec_error should never                                                                                                                                                               
!*** be invoked with any other.                                                                                                                                                                                                        
!***                                                                                                                                                                                                                                   
!*** ec_error:  generic error (catch-all, avoid!)                                                                                                                                                                                      
!*** ec_read:   parsing error during les()                                                                                                                                                                                             
!*** ec_dim:    dimensioning error                                                                                                                                                                                                     
!*** ec_log:    logic error                                                                                                                                                                                                            
!***                                                                                                                                                                                                                                   
!**** Inferred error codes                                                                                                                                                                                                             
!*** ec_dimrd:  ec_dim+ec_read                                                                                                                                                                                                         


      integer, parameter :: ec_error=1, ec_read=2, ec_dim=4, ec_log=8
      integer, parameter :: ec_dimrd=ec_dim+ec_read





      end module
