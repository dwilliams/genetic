      module fileread_mod
      contains
!-------------------------------------------------------------------

      subroutine get_datfile(datnam,dnlen)
      implicit none
!     Get name of input data file DATNAM either from the program's first
!     command line argument or ask the user.

      integer dnlen
      character(len=dnlen) datnam

      integer argcount

      argcount=iargc()
      if (argcount.gt.0) then
         call getarg(1,datnam)
      else
         write(6,'(A)') 'Specify input file:'
         read(*,*) datnam
      endif

      if (len_trim(datnam).eq.dnlen) then
         write(6,'(A)') 'ERROR: TRUNCATED FILENAME'
         write(6,'(A)') '"' // datnam // '"'
      endif

      end subroutine get_datfile

!-------------------------------------------------------------------

      subroutine internalize_datfile(datnam,infile,linenum,llen,
     >     maxlines,dnlen)
      use strings_mod,only:write_oneline,int2string
      implicit none

!     Read input file located at DATNAM, skipping comments and blank lines.
      integer dnlen,llen,maxlines
      integer linenum
      character(len=dnlen) datnam
      character(len=llen) infile(maxlines)

      character(len=llen) line

      !character*16 int2string

      integer j

      !Fabian
      character(len=llen) fmt,fmt2
      integer,parameter  ::  std_out = 6
      integer,parameter :: funit = 10
      write(fmt,'(A)') 'Reading file ''' // trim(datnam) // ''' ...'
      call write_oneline(fmt,std_out)

      open(unit=funit,file=datnam)
      linenum=0
      do j=1,maxlines
         !read(funit,fmt='(A<llen>)',end=20) line !<var> works only for ifort, not for gfortran or mpif90
         write(fmt2,'("(A",I3,")")') llen !Fabian
         read(funit,fmt=fmt2,end=20) line   !Fabian
         if (line(1:3).eq.'---') then
            write(fmt,'(A)') 'EOF-mark "---" found at line'
     >           // trim(int2string(j))
            call write_oneline(fmt,std_out)
            exit
         endif
         call internalize_line(linenum,infile,line,llen,maxlines)
      enddo
 20   close(funit)

      if (j.ge.maxlines) then
         write(6,'(A)') 'ERROR: FILE LENGTH EXCEEDING MAXLINES.'
         stop 1
      endif

      write(fmt,'(A)') 'File read successfully ('
     >     // trim(int2string(linenum)) // ' lines).'
      call write_oneline(fmt,std_out)

      end subroutine internalize_datfile

!-------------------------------------------------------------------

      subroutine internalize_line(linenum,infile,line,llen,maxlines)
      use strings_mod,only: strip_string,upcase
      implicit none
!     Parse a single line of input.  Ignore comments ("!..") and blank
!     lines,  and turn all input to uppercase.
!
!     infile:      data file's internalized form
!     line:        single verbatim line read from physical file
!     linenum:     current number of non-commentlines read
!                  increased by 1 if read line is not a comment
!     llen:        maximum character length of a single line
!     maxlines:    maximum number of lines in infile

      integer llen,maxlines
      integer linenum
      character(len=llen) infile(maxlines)
      character(len=llen) line

      character(len=llen) strip
      integer line_pos,text_end

      integer j

      line_pos=linenum+1

!     ignore empty lines
      if (len_trim(line).eq.0) then
         return
      endif

!     strip needless whitespace
      call strip_string(line,strip,llen)

!     determine EOL
!     ignore comments
      text_end=0
      do j=1,len_trim(strip)
         if (strip(j:j).eq.'!') then
            exit
         endif
         text_end=text_end+1
      enddo

      if (text_end.eq.llen) then
         write(6,'(A,I6)') 'WARNING: POTENTIALLY TRUNCATED LINE:'
         write(6,'(A)') '"' // strip(1:60) // '"...'
      endif

!     skip if line is a comment
      if (text_end.eq.0) then
         return
      endif

      infile(line_pos)=' '

!     turn string to uppercase and write to infile, ignoring comments
      call upcase(strip,infile(line_pos),text_end)

!     increment line number
      linenum=linenum+1

      end subroutine internalize_line

      end module
