      module long_keyread_mod
      contains

!     NOTE: all routines other than long_intkey and long_intline are
!     copy-pasted versions of different types.
!     replacements:
!     idat -> *dat
!     ipos -> *pos
!     istart -> *start
!     LONG_INT -> LONG_*

!---------------------------------------------------------------------------

      subroutine long_intkey(infile,inpos,key_end,idat,istart,
     >                       readlen,linelen,maxdat,maxlines)
      implicit none
!     Read an arbitrary number of integers for a single key from infile
!     and write to idat.
!
!     Data in infile is expected to have the general format
!
!     KEY: ... ... ... ... &
!     .... ... ... ... ... &
!     .... ... ... ... ...
!
!     Lines can be continued using the continuation marker arbitrarily
!     often.  A continuation marker at the last line causes the program
!     to read undefined data following below.  If that data is not a
!     valid line of integers, the program breaks appropiately.
!
!     idat:      vector to write read data on
!     istart:    current position in vector idat (first empty entry)
!     maxdat:    length of idat
!     readlen:   the number of read integers for current key
!
!     infile:    string vector containing the read input file linewise
!     key_end:   length of key, expected at the first line read
!     inpos:     current position in infile
!     linelen:   max. character length of a single line
!     maxlines:  length of infile
!
!     broken:    if true, assume read data to be corrupt
!     continued: if true, the next input line should continue
!                the current data block.


      integer maxlines,linelen,maxdat
      integer key_end
      integer istart,inpos,readlen
      integer idat(maxdat)
      character(len=linelen) infile(maxlines)
      logical continued, broken


      integer line_start,ipos
      character(len=linelen) key

      integer n

      ipos=istart
      readlen=0

      key=' '
      key=infile(inpos)(1:key_end)

!     skip key on first line
      line_start=key_end+1

      call long_intline(infile(inpos),linelen,line_start,
     >                  idat,ipos,maxdat,readlen,
     >                  continued,broken)

      line_start=1
      do n=inpos+1,maxlines
         if (broken) then
            continued=.false.
            exit
         endif
         if (.not.continued) then
            exit
         endif
         call long_intline(infile(n),linelen,line_start,
     >                     idat,ipos,maxdat,readlen,
     >                     continued,broken)
      enddo

      if (continued) then
         write(6,'(A)') 'ERROR: LONG_INTKEY: '
     >        // trim(key) //' CONTINUATION PAST EOF'
         write(6,'(A,I5.5)') 'LINE #',n
      endif
      if (broken) then
         write(6,'(A)') 'ERROR: LONG_INTKEY: '
     >        // trim(key) //' BROKEN INPUT.'
         write(6,'(A,I5.5)') 'LINE #',n
         write(6,'(A,I5.5,A)') 'AFTER ', n-inpos-1, ' CONTINUATIONS'
         stop 1
      endif


      end subroutine long_intkey

!---------------------------------------------------------------------------

      subroutine long_intline(inline,linelen,line_start,
     >                        idat,ipos,maxdat,readlen,
     >                        continued,broken)
      use strings_mod,only: count_words,nth_word
      implicit none
!     Read a single line of string input INLINE encoding integers.
!
!     idat:      vector to write read data on
!     ipos:      current position in vector idat (first empty entry)
!     maxdat:    length of idat
!     inline:    string containing line from read input file
!     linelen:   max. character length of a single line
!     broken:    if true, assume read data to be corrupt
!     continued: if true, the next input line should continue
!                the current data block.
!     readlen:   increment counting the number of read ints
!                ASSUMED TO BE INITIALIZED.

      integer linelen,maxdat
      integer line_start,ipos
      integer idat(maxdat)
      integer readlen
!     character(len=linelen) inline
      character(len=linelen) inline
      logical continued, broken

      integer line_end, wordcount
      character(len=linelen) workline, word

      integer n

      line_end=len_trim(inline)
      broken=.false.

!     check whether line will be continued
      if (inline(line_end:line_end).eq.'&') then
         continued=.true.
         line_end=line_end-1
      else
         continued=.false.
      endif

!     create working copy of line
      workline=' '
      workline=inline(line_start:line_end)

!     check the number of wordcount on line
      call count_words(workline,wordcount,linelen)

!     if the number of entries exceeds the length of idat, break
      if ((wordcount+ipos-1).gt.maxdat) then
         write(6,'(A)') 'ERROR: LONG_INTLINE: PARSER OUT OF MEMORY '
     >        // 'ON READ'
         write(6,'(A)') 'CURRENT LINE:'
         write(6,'(A)') trim(inline)
         broken=.true.
         return
      endif

      do n=1,wordcount
         call nth_word(workline,word,n,linelen)
         read(word,fmt=*,err=600,end=600) idat(ipos)
         readlen=readlen+1
         ipos=ipos+1
         cycle
!        avoid segfault in parser at all costs, throw error instead
 600     write(6,'(A,I4.4)') 'ERROR: LONG_INTLINE: '
     >        // 'A FATAL ERROR OCCURED ON ENTRY #',
     >        n
         write(6,'(A)') 'CURRENT LINE:'
         write(6,'(A)') trim(inline)
         broken=.true.
         return
      enddo

      end subroutine long_intline

!---------------------------------------------------------------------------

      subroutine long_realkey(infile,inpos,key_end,ddat,dstart,
     >                       readlen,linelen,maxdat,maxlines)
      implicit none
!     Read an arbitrary number of double precisions for a single key from infile
!     and write to ddat.
!
!     Data in infile is expected to have the general format
!
!     KEY: ... ... ... ... &
!     .... ... ... ... ... &
!     .... ... ... ... ...
!
!     Lines can be continued using the continuation marker arbitrarily
!     often.  A continuation marker at the last line causes the program
!     to read undefined data following below.  If that data is not a
!     valid line of integers, the program breaks appropiately.
!
!     ddat:      vector to write read data on
!     dstart:    current position in vector ddat (first empty entry)
!     maxdat:    length of ddat
!     readlen:   the number of read integers for current key
!
!     infile:    string vector containing the read input file linewise
!     key_end:   length of key, expected at the first line read
!     inpos:     current position in infile
!     linelen:   max. character length of a single line
!     maxlines:  length of infile
!
!     broken:    if true, assume read data to be corrupt
!     continued: if true, the next input line should continue
!                the current data block.


      integer maxlines,linelen,maxdat
      integer key_end
      integer dstart,inpos,readlen
      double precision  ddat(maxdat)
      character(len=linelen) infile(maxlines)
      logical continued, broken


      integer line_start,dpos
      character(len=linelen) key

      integer n

      dpos=dstart
      readlen=0

      key=' '
      key=infile(inpos)(1:key_end)

!     skip key on first line
      line_start=key_end+1

      call long_realline(infile(inpos),linelen,line_start,
     >                  ddat,dpos,maxdat,readlen,
     >                  continued,broken)

      line_start=1
      do n=inpos+1,maxlines
         if (broken) then
            continued=.false.
            exit
         endif
         if (.not.continued) then
            exit
         endif
         call long_realline(infile(n),linelen,line_start,
     >                     ddat,dpos,maxdat,readlen,
     >                     continued,broken)
      enddo

      if (continued) then
         write(6,'(A)') 'ERROR: LONG_REALKEY: '
     >        // trim(key) //' CONTINUATION PAST EOF'
         write(6,'(A,I5.5)') 'LINE #',n
      endif
      if (broken) then
         write(6,'(A)') 'ERROR: LONG_REALKEY: '
     >        // trim(key) //' BROKEN INPUT.'
         write(6,'(A,I5.5)') 'LINE #',n
         write(6,'(A,I5.5,A)') 'AFTER ', n-inpos-1, ' CONTINUATIONS'
         stop 1
      endif


      end subroutine long_realkey

!---------------------------------------------------------------------------

      subroutine long_realline(inline,linelen,line_start,
     >                        ddat,dpos,maxdat,readlen,
     >                        continued,broken)
      use strings_mod,only: count_words,nth_word
      implicit none
!     Read a single line of string input INLINE encoding double precisions.
!
!     ddat:      vector to write read data on
!     dpos:      current position in vector ddat (first empty entry)
!     maxdat:    length of ddat
!     inline:    string containing line from read input file
!     linelen:   max. character length of a single line
!     broken:    if true, assume read data to be corrupt
!     continued: if true, the next input line should continue
!                the current data block.
!     readlen:   increment counting the number of read ints
!                ASSUMED TO BE INITIALIZED.


      integer linelen,maxdat
      integer line_start,dpos
      integer readlen
      double precision  ddat(maxdat)
      character(len=linelen) inline
      logical continued, broken

      integer line_end, wordcount
      character(len=linelen) workline, word

      integer n

      line_end=len_trim(inline)
      broken=.false.

!     check whether line will be continued
      if (inline(line_end:line_end).eq.'&') then
         continued=.true.
         line_end=line_end-1
      else
         continued=.false.
      endif

!     create working copy of line
      workline=' '
      workline=inline(line_start:line_end)

!     check the number of wordcount on line
      call count_words(workline,wordcount,linelen)

!     if the number of entries exceeds the length of ddat, break
      if ((wordcount+dpos-1).gt.maxdat) then
         write(6,'(A)') 'ERROR: LONG_REALLINE: PARSER OUT OF MEMORY '
     >        // 'ON READ'
         write(6,'(A)') 'CURRENT LINE:'
         write(6,'(A)') trim(inline)
         write(6,*) 'wordcount',wordcount
         write(6,*) 'dpos',dpos
         write(6,*) 'maxdat',maxdat
         write(6,*) 'ddat',ddat(1:maxdat)
         broken=.true.
         return
      endif

      do n=1,wordcount
         call nth_word(workline,word,n,linelen)
         read(word,fmt=*,err=600,end=600) ddat(dpos)
         readlen=readlen+1
         dpos=dpos+1
         cycle
!        avoid segfault in parser at all costs, throw error instead
 600     write(6,'(A,I4.4)') 'ERROR: LONG_REALLINE: '
     >        // 'A FATAL ERROR OCCURED ON ENTRY #',
     >        n
         write(6,'(A)') 'CURRENT LINE:'
         write(6,'(A)') trim(inline)
         broken=.true.
         return
      enddo

      end subroutine long_realline

!---------------------------------------------------------------------------

      subroutine long_strkey(infile,inpos,key_end,cdat,cstart,
     >                       readlen,linelen,datlen,maxlines,clen)
      implicit none
!     Read an arbitrary number of strings for a single key from infile
!     and write to idat.
!
!     Data in infile is expected to have the general format
!
!     KEY: ... ... ... ... &
!     .... ... ... ... ... &
!     .... ... ... ... ...
!
!     Lines can be continued using the continuation marker arbitrarily
!     often.  A continuation marker at the last line causes the program
!     to read undefined data following below.  If that data is not a
!     valid line of strings, the program breaks appropiately.
!
!     cdat:      vector to write read data on
!     cstart:    current position in vector idat (first empty entry)
!     datlen:    length of idat
!     readlen:   the number of read integers for current key
!
!     infile:    string vector containing the read input file linewise
!     key_end:   length of key, expected at the first line read
!     inpos:     current position in infile
!     linelen:   max. character length of a single line
!     maxlines:  length of infile
!     clen:      maximum length of a given string
!
!     broken:    if true, assume read data to be corrupt
!     continued: if true, the next input line should continue
!                the current data block.
!     append:    if true, continue appending to an existing string.


      integer maxlines,linelen,datlen,clen
      integer key_end
      integer cstart,inpos,readlen
      character(len=linelen) infile(maxlines)
      character(len=clen) cdat(datlen)


      integer line_start,cpos
      integer strpos
      character(len=linelen) key
      logical continued, broken

      integer n

      cpos=cstart
      readlen=0

      key=' '
      key=infile(inpos)(1:key_end)

!     skip key on first line
      line_start=key_end+1

      strpos=0

      call long_strline(infile(inpos),linelen,line_start,
     >                  cdat,cpos,datlen,readlen,clen,
     >                  continued,broken,strpos)

      line_start=1
      do n=inpos+1,maxlines
         if (broken) then
            continued=.false.
            exit
         endif
         if (.not.continued) then
            exit
         endif
         call long_strline(infile(n),linelen,line_start,
     >                     cdat,cpos,datlen,readlen,clen,
     >                     continued,broken,strpos)
      enddo

      if (continued) then
         write(6,'(A)') 'ERROR: LONG_STRKEY: '
     >        // trim(key) //' CONTINUATION PAST EOF'
         write(6,'(A,I5.5)') 'LINE #',n
      endif
      if (broken) then
         write(6,'(A)') 'ERROR: LONG_STRKEY: '
     >        // trim(key) //' BROKEN INPUT.'
         write(6,'(A,I5.5)') 'LINE #',n
         write(6,'(A,I5.5,A)') 'AFTER ', n-inpos-1, ' CONTINUATIONS'
         stop 1
      endif


      end subroutine long_strkey


!---------------------------------------------------------------------------

      subroutine long_strline(inline,linelen,line_start,
     >                        cdat,cpos,datlen,readlen,clen,
     >                        continued,broken,strpos)
      use strings_mod,only:iswhitespace, downcase
      implicit none
!     Read a single line of string input INLINE encoding integers.
!
!     cdat:      vector to write read data on
!     cpos:      current position in vector cdat (first empty/incomplete entry)
!     datlen:    length of idat
!     inline:    string containing line from read input file
!     linelen:   max. character length of a single line
!     broken:    if true, assume read data to be corrupt
!     continued: if true, the next input line should continue
!                the current data block.
!     readlen:   increment counting the number of read strings
!                ASSUMED TO BE INITIALIZED.
!     strpos:    if 0, create new string. Otherwise, append to string of assumed
!                length strpos.

      integer :: linelen,datlen,clen
      integer :: line_start,cpos,strpos
      integer :: readlen
      character(len=linelen) :: inline
      character(len=clen) :: cdat(datlen)
      logical :: continued, broken

      character,parameter ::  esc = ACHAR(92) ! "\"

      integer :: line_end
      character(len=linelen) ::  workline
      character(len=1) ::  char, tmp_char

      logical :: cont_string, escaped

      integer :: j

!      logical :: iswhitespace

      broken=.false.
      continued=.false.
      cont_string=.false.
      escaped=.false.

!     create working copy of line
      workline=' '
      workline=inline(line_start:len_trim(inline))
      line_end=len_trim(workline)

!     If needed, initialize working position in cdat
      if (strpos.eq.0) then
         cdat(cpos)=' '
      endif

!     iterate over characters in line
      do j=1,line_end
         char=workline(j:j)
         if (escaped) then
!           Insert escaped character and proceed.
            escaped=.false.
!           Special escape sequences
            if (char.eq.'.') then
!              \. = !
               char='!'
            endif
         else if (char.eq.esc) then
!           Consider next character escaped, skip char.
            escaped=.true.
            cycle
         else if (char.eq.'&') then
            continued=.true.
            if (j.eq.line_end) then
               exit
            endif
!           Deal with unusual continuations, look at char after "&"
            char=workline(j+1:j+1)
            if (char.eq.'&') then
!              "&&" allows multi-line strings
               cont_string=.true.
               if (j+1.eq.line_end) then
                  exit
               endif
            endif
            write(6,'(A)') 'WARNING: LONG_STRLINE: IGNORED'
     >           // ' JUNK CHARACTER(S) FOLLOWING'
     >           // ' CONTINUATION CHARACTER.'
            exit
         else if (iswhitespace(char)) then
!           Whitespace separates strings; skip char.
            if (strpos.gt.0) then
!              Begin a new string unless the current one is empty.
               strpos=0
               cpos=cpos+1
               cdat(cpos)=' '
            endif
            cycle
         else
!           assume char to be meant as a downcase char
            call downcase(char,tmp_char,1)
            char=tmp_char
         endif

!        Incorporate new char into string
         strpos=strpos+1

!        Break if a boundary exception occurs
         if (cpos.gt.datlen) then
            write(6,'(A)') 'ERROR: LONG_STRLINE: PARSER OUT OF MEMORY'
     >           // ' ON READ'
            write(6,'(A)') 'CURRENT LINE:'
            write(6,'(A)') trim(inline)
            broken=.true.
            return
         else if (strpos.gt.clen) then
            write(6,'(A)') 'ERROR: LONG_STRLINE: PARSER OUT OF MEMORY'
     >           // ' ON READ: STRING ARGUMENT EXCEEDS CLEN'
            write(6,'(A)') 'CURRENT LINE:'
            write(6,'(A)') trim(inline)
            broken=.true.
            return
         endif

!        insert character
         cdat(cpos)(strpos:strpos)=char
         if (strpos.eq.1) then
            readlen=readlen+1
         endif
      enddo

!     Fix incomplete escape sequences and deal with continuation
      if (escaped) then
         write(6,'(A)') 'WARNING: LONG_STRLINE: ENCOUNTERED ESCAPE'
     >        // ' CHARACTER AT EOL. IGNORED.'
      endif

!     Unless the line ended with "&&", consider the current, non-empty
!     string complete.
      if ((cont_string).or.(strpos.eq.0)) then
         return
      else
         cpos=cpos+1
         strpos=0
      endif

      end subroutine long_strline

      end module
