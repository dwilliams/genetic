      module parse_errors
      use io_parameters, only:
     >   keylist, errcat, ec_dim, ec_log, ec_read, ec_error
      implicit none

      contains

!--------------------------------------------------------------------------------

      subroutine signal_p_error(key_id,msg)
!     Signal generic error with user-defined message MSG.
      integer key_id
      character(len=*) msg

      write(6,'(A)') 'ERROR: ' // trim(keylist(1,key_id))
     >     // ' ' // trim(msg)
      stop ec_error

      end subroutine

!--------------------------------------------------------------------------------

      subroutine signal_dim_error(key_id,msg_code,value,expval)
      use strings_mod,only:int2string
!     Signals errors where one specific dimensioning value is ill-set.
!     If the optional parameter EXPVAL is given, return it as expected
!     dimensioning value.
      integer key_id, value
      integer, optional :: expval
      integer msg_code
      write(6,'(A)') 'ERROR: ' // trim(keylist(1,key_id))
     >     // ' ' // trim(errcat(msg_code))
      write(6,'(A)') 'OFFENDING VALUE: ' // trim(int2string(value))
      if (present(expval)) then
         write(6,'(A)') 'EXPECTED: ' // trim(int2string(expval))
      endif
      stop ec_dim

      end subroutine

!--------------------------------------------------------------------------------

      subroutine signal_log_error(key_id,msg_code,alt_key)
!     Signals errors where contradictory settings are provided which
!     the program cannot resolve.  If the optional parameter ALT_KEY
!     is given, name the explicit key current settings clash with.
      integer key_id
      integer, optional :: alt_key
      integer msg_code

      write(6,'(A)') 'ERROR: ' // trim(keylist(1,key_id))
     >     // ' ' // trim(errcat(msg_code))
      if (present(alt_key)) then
         write(6,'(A)') 'OFFENDING KEY: ' // trim(keylist(1,alt_key))
      endif

      stop ec_log

      end subroutine

!--------------------------------------------------------------------------------

      subroutine signal_val_error(key_id,msg_code,value,expval)
      use strings_mod,only:int2string
!     Signals errors where a given value makes no sense in it's given context.
!     If the optional parameter EXPVAL is given, return it as expected
!     dimensioning value.
      integer key_id, value
      integer, optional :: expval
      integer msg_code
      write(6,'(A)') 'ERROR: ' // trim(keylist(1,key_id))
     >     // ' ' // trim(errcat(msg_code))
      write(6,'(A)') 'OFFENDING VALUE: ' // trim(int2string(value))
      if (present(expval)) then
         write(6,'(A)') 'EXPECTED: ' // trim(int2string(expval))
      endif
      stop ec_read

      end subroutine

!--------------------------------------------------------------------------------

      subroutine signal_dval_error(key_id,msg_code,value,expval)
      use strings_mod,only: shortdble2string
!     Signals errors where a given value makes no sense in it's given context.
!     If the optional parameter EXPVAL is given, return it as expected
!     dimensioning value.
      integer key_id
      double precision  value
      double precision, optional :: expval
      integer msg_code
      write(6,'(A)') 'ERROR: ' // trim(keylist(1,key_id))
     >     // ' ' // trim(errcat(msg_code))
      write(6,'(A)') 'OFFENDING VALUE: '
     >     // trim(shortdble2string(value))
      if (present(expval)) then
         write(6,'(A)') 'EXPECTED: ' // trim(shortdble2string(expval))
      endif
      stop ec_read

      end subroutine


!--------------------------------------------------------------------------------

      subroutine signal_meta_error(key_id,msg_code)
!     Signals errors where a key (or key combinations) is/are not
!     supported or maintained for reasons outside of the program's
!     scope (e.g.: deprecation).
      integer key_id,msg_code

      write(6,'(A)') 'ERROR: ' // trim(keylist(1,key_id))
     >     // ' ' // trim(errcat(msg_code))
      stop ec_read

      end subroutine
      end module
