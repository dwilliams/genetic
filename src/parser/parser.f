
!     >Module Containing Subroutines relevant for reading cards and information from an inputfile

      module parser
      use io_parameters
      use dim_parameter
      use parse_errors
      use parameterkeys, only: parameterkey_read
      use long_write
      implicit none
      contains
!--------------------------------------------------------------------------------------------------------------------------------------
!     >Reads Cards and Data from Inputfile
!     !@param datname name of input file that is readed
!     !@param infile internalized input file
!     !@param linenum linenumber of internalized input file
!     !@param idat
      subroutine les(x,y,wt,p,p_act,p_spread,npar,
     >     seed,gtype,nset,maxit,micit,nsel,mut,difper,freeze)
      use strings_mod,only:write_oneline
      use fileread_mod,only:get_datfile,internalize_datfile
      use keyread_mod,only:keyread
!      implicit none
!     Include Files for needed dimension parameters

!     Declare OUT Variables
!     Data variables
      double precision, allocatable :: x(:,:) , y(:,:), wt(:,:)
!     Fiting Model Parameters
      double precision, allocatable :: p(:) !< vector(npar) for the values of read parameters
      integer, allocatable :: p_act(:) !< vector(npar) containing 0 or 1 defining if corresponding parameters are activ in Fit
      double precision, allocatable :: p_spread(:) !< vector(npar) for the spread values for each parameter
      integer npar              !< read length of parameter arrays
!     Fit control Parameters
      integer seed              !< Seed for RNG
      integer nset              !< number of diffrent parameter sets
      logical freeze            !< determines if parameters are active
      double precision  mut, difper       !< percentage of selected Parents, number of mutations in parents, minimum diffrence between selected parents
      double precision  psel              !< percantage of selected parents
      integer nsel              !< number of selected parents , generated from psel and nset by rounding to nearest integer
      integer gtype             !< type of RNG used
      integer maxit, micit      !<maximum makro and micro iterations for the genetic program
!     weighting parameters

!     Declare INTERNAL variables
      character(len=dnlen) :: datname, dbgdatname !< name of the input File
      character(len=llen), dimension(:), allocatable :: infile !< internalized array of capitalized input file without comments, blanklines
      integer  linenum          !< linenumber in infile
      double precision gspread
!     data arrays
      integer idat(maxdat)
      double precision  ddat(maxdat)
      character(len=clen) cdat(maxdat)
!     minimum ntot (inferred from ndiab etc)
      integer min_ntot


!     running index
      integer j                 !< running index

!     general key variables
      integer key_id            !< integer identifying a key from keylist.incl
      logical legacy_wt

!     length or position variables
      integer dat_start         !< linenumber in infile where DATA: Block starts

!     Fabian
      character(len=llen) :: fmt,fmt2
      integer, parameter  :: id_internal = 10 ! hardcoded until queue is ready for modern features
      integer, parameter :: std_out = 6

!     allocate relevant arrays
      allocate(infile(maxlines))

!     define Error Messages
      include 'errcat.incl'

!     include general keylist
      include 'keylist.incl'
      do j=1,maxkeys
         if (keylist(1,j)(1:1).eq.' ') then
            keynum=j-1
            write(fmt,'("Number of accepted input keys: ",I3)') keynum
            call write_oneline(fmt,std_out)
            exit
         endif
      enddo

!############################################################
!     Read input file
!############################################################

      call get_datfile(datname,dnlen)
      call internalize_datfile
     >     (datname,infile,linenum,llen,maxlines,dnlen)
      dbgdatname='.internal_input'
#ifndef mpi_version
      write(6,'(A)') 'Writing internalized version of input to '''
     >     // trim(dbgdatname) // '''..'
      open(unit=id_internal,file=trim(dbgdatname))
      do j=1,linenum
         write(id_internal,'(A)') trim(infile(j))
      enddo
      close(id_internal)
#endif
      write(fmt,'("Parsing Keys..")')
      call write_oneline(fmt,std_out)

      call keyread(keylist,infile,keynum,idat,ddat,cdat,datpos,
     >     klen,llen,clen,linenum,maxdat)

!############################################################
!     Read Individual keys for Program Control
!############################################################

!************************************************************************
!     DATA:
!************************************************************************
!     This card separates the data to be fitted from the rest of the
!     file.
!************************************************************************
      key_id=1
!     Find where in the input file the DATA:-block begins and
!     exclude the line of the card itself
      dat_start=datIdx(2,key_id)

!************************************************************************
!     SEED:
!************************************************************************
!     Random seed for the RNG.
!************************************************************************
      key_id=2
      seed=8236475

      if (is_present(key_id)) then
         seed=idat(datIdx(1,key_id))
      else
         write(fmt,76) seed
         call write_oneline(fmt,std_out)
      endif
 76   format('No random seed specified; seed set to',i12)

      if (abs(seed).lt.10**5) then
         call signal_val_error(key_id,5,seed)
      endif

      write(fmt,'("Random seed set to: ",I12)') seed
      call write_oneline(fmt,std_out)

      seed=-iabs(seed)

!************************************************************************
!     NSET:
!************************************************************************
!     Number of diffrent Parameter sets.
!************************************************************************
      key_id=3
      nset=1
      if (is_present(key_id)) then
         nset=idat(datIdx(1,key_id))
         if (nset.le.0)
     >        call signal_val_error(key_id,5,nset)
      else
         write(fmt,77) nset
         call write_oneline(fmt,std_out)
      endif
 77   format('No number of Parametersets specified; nset set to',i9)

      write(fmt,'("Number of Parametersets set to: ",I9)') nset
      call write_oneline(fmt,std_out)

!************************************************************************
!     FREEZE:
!************************************************************************
!     Determines if All parameters are nonactive if present.
!************************************************************************
      key_id=4
      freeze=is_present(key_id)

!************************************************************************
!     NSTAT:
!************************************************************************
!     Number of Energievalues in y for each Point
!************************************************************************
      key_id=5
      nstat = idat(datIdx(1,key_id))

      write(fmt,'("Number of Energie values set to: ",I9 )') nstat
      call write_oneline(fmt,std_out)

!************************************************************************
!     NCI:
!************************************************************************
!     Number of CI vectors in y for each Geometry
!************************************************************************
      key_id=6
      nci = 0
      if(is_present(key_id)) then
         nci =idat(datIdx(1,key_id))
      endif
      write(fmt,'("Number of CI vectors set to: ",I9 )') nci
      call write_oneline(fmt,std_out)

!************************************************************************
!     NDIAB:
!************************************************************************
!     Size of diabatic space = lenght of ci vectors
!************************************************************************
      key_id=7
      ndiab=nstat
      if(is_present(key_id)) then
         ndiab = idat(datIdx(1,key_id))
      endif

      write(fmt,'("Setting ndiab to:",I9)') ndiab
      call write_oneline(fmt,std_out)

!************************************************************************

      min_ntot= nstat + (nci*ndiab)
      if(min_ntot.gt.max_ntot) then
         write(6,*)'ERROR: ntot exceeds set Maximum: ',min_ntot,max_ntot
         stop
      endif

!************************************************************************
!     HYBRID:
!************************************************************************
!     If present then CI vectors are used in Fit
!************************************************************************
      key_id=8
      hybrid=is_present(key_id)
      if(hybrid.and.(nci.le.0)) then
         write(6,*) 'Cant do Hybrid Fit without ci vectors, nci: ',nci
         stop ec_log
      endif

!************************************************************************
!     SEL:
!************************************************************************
!     Percentage of selected Parameter sets as Parents
!************************************************************************
      key_id=9
      psel=0.15d0

      if(is_present(key_id)) then
         psel = ddat(datIdx(1,key_id))
         if (psel.gt.1.d0) call signal_dval_error(key_id,7,psel*100)
      endif
      nsel=max(int(psel*nset),1)

      write(fmt,79) psel*100, nsel
      call write_oneline(fmt,std_out)

 79   format(f5.1,'%(#',i5,')of Parameters will be selected as parents')

!************************************************************************
!     MUT:
!************************************************************************
!     Percentage of how many mutations happen in parameters
!************************************************************************
      key_id=10
      mut=0.d0
      if(is_present(key_id)) then
         mut = ddat(datIdx(1,key_id))
         if (mut.gt.1.d0) call signal_dval_error(key_id,7,mut*100.d0)
      endif

      write(fmt,80) mut
      call write_oneline(fmt,std_out)

 80   format('MUTATION set to: ',g9.1)

!************************************************************************
!     DIFPER:
!************************************************************************
!     minimum Percentage of diffrence between selected parents
!************************************************************************
      key_id=11
      difper=0.05d0
      if(is_present(key_id)) then
         difper = ddat(datIdx(1,key_id))
         if (difper.gt.1.d0) then
            call signal_dval_error(key_id,7,difper*100.d0)
         endif
      endif

      write(fmt,81) difper
      call write_oneline(fmt,std_out)

 81   format('DIFPER set to: ',g9.1)

!************************************************************************
!     GTYPE:
!************************************************************************
!     Type of used RNG
!************************************************************************
      key_id=12
      gtype=2
      if(is_present(key_id)) then
         gtype = idat(datIdx(1,key_id))
      endif

      write(fmt,'("GTYPE set to: ",i9)') gtype
      call write_oneline(fmt,std_out)

!************************************************************************
!     MAXIT:
!************************************************************************
!     number of maximum makro Iterations
!************************************************************************
      key_id=13
      maxit=5
      if(is_present(key_id)) then
         maxit=idat(datIdx(1,key_id))
      endif

      write(fmt,'("max. number of makro iterations set to: ",i9)') maxit
      call write_oneline(fmt,std_out)

!************************************************************************
!     MICIT:
!************************************************************************
!     number of maximum micro Iterations
!************************************************************************
      key_id=14
      micit=1000
      if(is_present(key_id)) then
         micit=idat(datIdx(1,key_id))
      endif

      write(fmt,'("max. number of micro iterations set to: ",i9)') micit
      call write_oneline(fmt,std_out)

!************************************************************************
!     GSPREAD:
!************************************************************************
!     read general Spread for Parameter keys
!************************************************************************
      key_id=15
      gspread=1.d0
      if(is_present(key_id)) then
         gspread = ddat(datIdx(1,key_id))
      endif

      write(fmt,'("General Parameterspread set to: ",f5.2)') gspread
      call write_oneline(fmt,std_out)

!************************************************************************
!     SETS:
!************************************************************************
!     Number of seperatly grouped geometries.
!     With more than one argument, total sets = sum of all entries.
!************************************************************************
      key_id=16
      sets=-1
      sets=idat(datIdx(1,key_id))
      do j=2,datlen(key_id)
         sets=sets+idat(datIdx(j,key_id))
      enddo

      if(sets.eq.0) call signal_val_error(key_id,5,sets,1)

      write(fmt,'("Number of Data Sets set to: ",i9)') sets
      call write_oneline(fmt,std_out)

!************************************************************************
!     INPUTS:
!************************************************************************
!     Dimension of input values.
!     INPUTS: D [d]
!     If given the optional second argument d, read d<D coordinates off
!     the DATA: block.
!************************************************************************
      key_id=17
      qn=-1
      qn=idat(datIdx(1,key_id))
      if (datlen(key_id).eq.1) then
         qn_read=qn
      else if (datlen(key_id).eq.2) then
         qn_read=idat(datIdx(2,key_id))
         if (qn_read.gt.qn) then
            call signal_val_error(key_id,4,qn_read,qn)
         else if (qn_read.le.0) then
            call signal_val_error(key_id,5,qn_read,1)
         endif
      else if (datlen(key_id).gt.2) then
         call signal_dim_error(key_id,11,datlen(key_id),2)
      endif

      if(qn.le.0) call signal_val_error(key_id,5,qn,1)

!************************************************************************
!     ENCIRATIO
!************************************************************************
!     parameter used for weighting ratio between energies and CI vectors
!************************************************************************
      key_id=18
      if(nci.gt.0) then
         wt_en2ci=1./(ndiab+0.d0)
      else
         wt_en2ci=1.d0
      endif

      if(is_present(key_id)) then
         wt_en2ci=ddat(datIdx(1,key_id))
      endif

      write(fmt,82) wt_en2ci
      call write_oneline(fmt,std_out)

 82   format('Setting Ratio between Energie and CI Weights to:',g9.1)

!************************************************************************
!     WTEN:
!************************************************************************
!     parameter used for weighting states independent
!************************************************************************
      key_id=19
      allocate(wt_en(nstat))
      wt_en=1.d0

      if(is_present(key_id)) then
         if(datlen(key_id).ne.nstat)
     >        call signal_dim_error(key_id,3,datlen(key_id),nstat)
         do j=1,nstat
            wt_en(j)=ddat(datIdx(j,key_id))
         enddo
      endif

!************************************************************************
!     WTCI:
!************************************************************************
!     parameter used for weighting CI vectors independent
!************************************************************************
      key_id=20
      allocate(wt_ci(nci))
      wt_ci=1.d0

      if(is_present(key_id)) then
         if(datlen(key_id).ne.nstat)
     >        call signal_dim_error(key_id,3,datlen(key_id),nci)
         do j=1,nci
            wt_ci(j)=ddat(datIdx(j,key_id))
         enddo
      endif

!************************************************************************
!     RMSTHR:
!************************************************************************
!     Threshhold for RMSE calculation for cutting above the given threshold
!     one or nstat real expected for each energie one threshold or one for all
!************************************************************************
      key_id=23
      allocate(rms_thr(nstat))
      rms_thr = 0.d0

      if(is_present(key_id)) then
         if(datlen(key_id).eq.nstat) then
            do j=1,nstat
               rms_thr(j)=ddat(datIdx(j,key_id))
            enddo
!            write(6,'("Setting RMS Threshold for individual States to: ",
!     ><nstat>g12.4)') rms_thr(1:nstat) !<var> works only for ifort, not for gfortran or mpif90

            write(fmt2,'("(A,",I2,"g12.4)")') nstat
            write(fmt,fmt2)
     $           "Set RMS Threshold for individual states to:",
     $           rms_thr(1:nstat)
            call write_oneline(fmt,std_out)

         else if (datlen(key_id).eq.1) then
            rms_thr = ddat(datIdx(1,key_id))
!            write(6,'("Setting RMS Threshold for all States to: ",
!     ><nstat>g12.4)') rms_thr !<var> works only for ifort, not for gfortran or mpif90
            write(fmt2,'("(A,",I2,"g12.4)")') nstat
            write(fmt,fmt2)
     $           "Set RMS Threshold for individual states to:",
     $           rms_thr(1:nstat)
            call write_oneline(fmt,std_out)

         else
            call signal_dim_error(key_id,3,datlen(key_id),nstat)
         endif
      endif


!************************************************************************
!     NPOINTS:
!************************************************************************
!     Number of geometries for each set
!************************************************************************
      key_id=21
      allocate(ndata(sets))
      ndata=0

      if (is_present(key_id)) then
         if (datlen(key_id).ne.sets) then
            call signal_dim_error(key_id,3,datlen(key_id),sets)
         endif
         do j=1,sets
            ndata(j)=idat(datIdx(j,key_id))
         enddo
         numdatpt=sum(ndata(1:sets))
      else
         write(*,*)'WARNING: NO NPOINTS CARD GIVEN'
      endif

!************************************************************************
!     NTOT:
!************************************************************************
!     Total number of output values.
!************************************************************************
      key_id=22
      ntot=min_ntot
      if (is_present(key_id)) then
         ntot=idat(datIdx(1,key_id))
         if(ntot.lt.min_ntot) then
            write(6,*)'ERROR: ntot less than set Minimum: ',
     >           ntot,min_ntot
            stop
         elseif(ntot.gt.max_ntot) then
            write(6,*)'ERROR: ntot exceeds set Maximum: ',ntot,max_ntot
            stop
         endif
      endif


!************************************************************************
!     ANAGRAD:
!************************************************************************
!     if present analytical gradients are used for eigenvalues and vectors
!************************************************************************
      key_id=24
      anagrad=is_present(key_id)
      if(anagrad) then
         write(fmt,'(A)') 'Using Analytical gradients.'
         call write_oneline(fmt,std_out)
      endif

!************************************************************************
!     LBFGS:
!************************************************************************
!     if present the LBFGS-B algorithm of Nocedal and Wright is used
!     instead of the default Levenberg-Marquard algorithm
!************************************************************************
      key_id=25
      lbfgs=is_present(key_id)
      if(lbfgs) then
         write(fmt,'(A)') 'Using LBFGS-B algorithm for fit'
         call write_oneline(fmt,std_out)
      endif

      key_id=26
      lbfgs_corr=10             !Standard value
      if (lbfgs) then
         if(is_present(key_id)) then
            lbfgs_corr=idat(datIdx(1,key_id))
         endif
         if(lbfgs_corr.eq.0)
     $        call signal_val_error(key_id,5,lbfgs_corr,1)
         write(fmt,'("Number of LBFGS corrections set to: ",i9)')
     $        lbfgs_corr
         call write_oneline(fmt,std_out)
      endif

!************************************************************************
!     FACSPREAD:
!************************************************************************
!     read multiplicative factor for spreads of all parameters
!************************************************************************
      key_id=27
      facspread=1.d0
      if(is_present(key_id)) then
         facspread = ddat(datIdx(1,key_id))
         if(facspread.le.0.d0) then
            write(6,*) 'ERROR: facspread <= 0'
            stop
         endif
      endif

      write(fmt,'("Multiplicative factor for parameter spread: ",f5.2)')
     $     facspread
      call write_oneline(fmt,std_out)

!************************************************************************
!     LOGCONVERGENCE:
!************************************************************************
!     If present logging files for convergence are printed
!************************************************************************
      key_id=28
      log_convergence=is_present(key_id)

!************************************************************************
!     COORD:
!************************************************************************
!     For each set, specify a coord number N, where
!     N=0 (default) computes a walk coordinate on q mapped to [0:1]
!     N>0 plot against q(N)
!
!************************************************************************
      key_id=29
      allocate(plot_coord(sets))
      plot_coord=0
      if (is_present(key_id)) then
         if (datlen(key_id).ne.sets) then
            call signal_dim_error(key_id,3,datlen(key_id),sets)
         endif
         do j=1,sets
            plot_coord(j)=idat(datIdx(j,key_id))
         enddo
         fmt='COORD: Scan file(s) will use the following coordinates:'
         call write_oneline(fmt,std_out)
         fmt='(I3)'
         call write_longint(std_out,plot_coord,datlen(key_id),
     >        fmt,16)
      endif

!************************************************************************
!     PARMETER KEYS:
!************************************************************************
!     read the parameter keys defined in keys.incl
!************************************************************************

!     TODO: automate parameter key generation.

      call parameterkey_read
     >     (infile,linenum,p,p_act,p_spread,npar,gspread,facspread)

      if (all(p_act.eq.0)) then
         write(std_out,'(A)') 'WARNING: No active parameters. '
     >        // 'Setting FREEZE:'
         freeze=.true.
      endif


!************************************************************************
!     DATA:
!************************************************************************
!     reading x an y values in the datablock after DATA: card
!************************************************************************
      legacy_wt=.true. !< @TODO consider implementing card for ANN weighting format 
      call read_data(infile,x,y,wt,
     >     legacy_wt,dat_start,linenum,ntot,qn,
     >     qn_read,numdatpt)


      deallocate(infile)
      end subroutine

!************************************************************************
      subroutine read_data(in,x,y,wt,
     >     legacy_wt,st,lauf,y_dim,x_dim,
     >     x_read,ndatapoints)
!     Routine reading DATA-block.
!     If ndatapoints is nonzero, only the first ndatapoints pattern pairs are read.
!
!     in:          input file as string vector
!       in(n)        nth line of input file
!     lauf:        number of lines in input file
!     st:          starting position of DATA-block
!
!.....Splitting variables
!     ndatapoints:        number of given pattern pairs
!     nref:        number of reference patterns
!.....Data arrays containing the read out and in values
!     wterr:       weight factors for each element of the error vector e
!     x:    input patterns
!     y:   desired output patterns
!      x/y(i,N):   value of ith in-/output neuron for pattern N
!     x_dim:  physical dimension of x(:,N)
!     x_read: number of read coordinates (rest is 0)
!
!     expected format (for one pattern pair):
!..      y1 x1 x2 x3 ... xM
!..      y2 x1 x2 x3 ... xM
!..      .. .. .. .. ... ..
!..      yN x1 x2 x3 ... xM
!..
!..      WT: w1 w2 ... wN
!
!...  wt-legacy mode format:
!..      y1 x1 x2 x3 ... xM
!..      WT: w1
!..      y2 x1 x2 x3 ... xM
!..      WT: w2
!..      .. .. .. .. ... ..
!..      yN x1 x2 x3 ... xM
!..      WT: wN
!
!     where N=inp_out and M=inp_in

      double precision, allocatable ::  x(:,:),y(:,:)
      double precision, allocatable ::  wt(:,:)
!     actual relevant Dimensions
      integer ndatapoints,st,lauf,y_dim,x_dim
      integer x_read
      character(len=llen) in(lauf)
      logical legacy_wt
      integer pat_count,line

      integer k

!     allocate arrays
      allocate(x(x_dim,ndatapoints),y(y_dim,ndatapoints),
     >     wt(y_dim,ndatapoints))


      pat_count=0
      line=st  !count lines

      do while (line.le.lauf)
         if (in(line)(1:3).eq.'WT:') then

            if (legacy_wt .or. (pat_count.eq.0)) then
               write(6,419) 1
               write(6,'(A)') '(preceding WT-block)'
               stop ec_read
            endif

            read(in(line)(4:llen),*,err=511,end=508)
     >           wt(1:y_dim,pat_count)

            line=line+1

            if (pat_count.eq.ndatapoints) exit

            cycle
 508        write(6,419) pat_count
            write(6,'(A)') '(broken WT: input)'
            stop ec_read
 511        write(6,418) pat_count
            write(6,'(A)') 'LINE DUMP:'
            write(6,'(A)') trim(in(line)(4:llen))
            stop ec_read
         else
!           stop reading if desired number of patterns is read
            if ((ndatapoints.gt.0).and.(pat_count.eq.ndatapoints)) exit

!           new input set begins
            pat_count=pat_count+1
            wt(1:y_dim,pat_count)=1.0D0
            x(:,pat_count)=0.d0
            read(in(line)(1:llen),*,err=513,end=510) y(1,pat_count),
     >                                  x(1:x_read,pat_count)
            line=line+1
!           wt-legacy-mode: read single weight
            if (legacy_wt .and. (in(line)(1:3).eq.'WT:')) then
               read(in(line)(4:llen),*,err=515,end=514)
     >              wt(1:1,pat_count)
               line=line+1
            endif

            do k=2,y_dim
!              read y(k,pat_count) and copy x-vector for comparison
               read(in(line)(1:llen),*,err=512,end=509)
     >              y(k,pat_count)

               if (line.lt.lauf) then
                  line=line+1
                  if (legacy_wt .and. (in(line)(1:3).eq.'WT:')) then
                     read(in(line)(4:llen),*,err=515,end=514)
     >                    wt(k:k,pat_count)
                     line=line+1
                  endif
                  cycle
               else if (k.eq.y_dim) then
                  exit
               endif
 509           write(6,419) pat_count
               write(6,'(A)') '(reached EOF before completion)'
               stop ec_read
 512           write(6,421) pat_count, line
               write(6,'(A)') 'LINE DUMP:'
               write(6,'(A)') trim(in(line)(1:llen))
               stop ec_read
            enddo

            cycle
 510        write(6,419) pat_count
            stop ec_read
 513        write(6,421) pat_count, line
            write(6,'(A)') 'LINE DUMP:'
            write(6,'(A)') trim(in(line)(1:llen))
            stop ec_read
 514        write(6,419) pat_count
            write(6,'(A)') '(broken WT: input)'
            stop ec_read
 515        write(6,418) pat_count
            write(6,'(A)') 'LINE DUMP:'
            write(6,'(A)') trim(in(line)(4:llen))
            stop ec_read
         endif
      enddo
!     pat_count is now actual number of patterns

      if (pat_count.le.0) then
         write(6,419) 1
         stop ec_read
      else if (ndatapoints.ne.pat_count) then
         write(6,420) ndatapoints,pat_count
         stop ec_read
      endif


! 417  format('ERROR: CORRUPTED WEIGHT INPUT (SET #',I9,')')
 418  format('ERROR: NUMDATPT EXCEEDING MAX_NUMDATPT(',I9,' vs.',I9,')')
 419  format('ERROR: INCOMPLETE OR MISSING DATA SET. SET #',I9)
 420  format('ERROR: NUMBER OF DATA POINTS INCONSISTENT
     > WITH NDATAPOINTS',
     >       '(',I9,' vs.',I9,')')
 421  format('ERROR: CORRUPTED DATA POINT INPUT (SET #',I9,', LINE,',
     >     I9,')')

      end subroutine

!--------------------------------------------------------------------------------
!     Here follow convenience functions defined for this modul only.

      integer function datIdx(j,key_id)
!     Locate Jth value of KEY_IDth data block on *dat vector(s).

      integer j,key_id

      datIdx=IdxShift(j,datpos(2,key_id))
      end function

!--------------------------------------------------------------------------

      integer function IdxShift(j,start)
!     Map linear index of a logical vector which is embedded in a memory
!     vector and begins at START.

      integer j,start

      IdxShift=start-1+j

      end function

!--------------------------------------------------------------------------------

      logical function is_present(key_id,quiet)
      use strings_mod,only:write_oneline
      implicit none
!     Checks whether optional key has been given in input file.
!     If optional argument QUIET is true, do not print a message
!     if the key wasn't found.

      integer key_id
      logical quiet
      optional quiet

      character(len=llen) fmt
      integer,parameter  ::  std_out = 6

      is_present=(datpos(2,key_id).ne.-1)

      if (present(quiet)) then
         if (quiet) then
            return
         endif
      else if (.not.is_present) then
         write(fmt,'(A)') 'No '//trim(keylist(1,key_id))//' card found.'
         call write_oneline(fmt,std_out)
      endif

      end function

!----------------------------------------------------------------------------------
      integer function datlen(key_id)
      implicit none
      integer key_id
      datlen=datpos(3,key_id)
      end function

      end module
